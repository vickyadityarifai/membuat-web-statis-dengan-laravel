@extends('master')

@section('judul')
    Daftar Film
@endsection

@section('content')
<div>

@auth
<a href="/film/create" class="btn btn-danger my-2">Tambah</a>
@endauth
<div class="row mx-1">
    @foreach ($film as $item)
    <div class="card mx-1">
      <img src="{{ asset('/img/'.$item->poster) }}" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">{{$item->judul}} ({{$item->tahun}})</h5>
        <p class="card-text">{{Str::limit($item->ringkasan, 50)}}</p>
        <form action="/film/{{$item->id}}" method="POST">
          @method('delete')
          @csrf
          <a href="/film/{{$item->id}}" class="btn btn-primary">Read More</a>
@auth
          <a href="/film/{{$item->id}}/edit" class="btn btn-info">Update</a>
          <input type="submit" class="btn btn-danger" value="Delete">
@endauth
        </form>
        <small>{{$item->created_at}}</small>
      </div>
    </div>
    @endforeach
</div>
</div>
@endsection