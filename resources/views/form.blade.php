<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Index</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
            <h1>Buat Account Baru!</h1>
		<h2>Sign Up Form</h2>
		<form method="post" action="/welcome">
			@csrf
			<p>First name :</p>
			<input type="text" name="firstname">
			<p>Last name :</p>
			<input type="text" name="lastname">
			<p>Gender :</p>
			<input type="radio" id="male" name="gender" value="Male">Male<br>
			<input type="radio" id="male" name="gender" value="Male">Female<br>
			<input type="radio" id="male" name="gender" value="Male">Other<br>
			<p>Nationality :</p>
			<select name="nationality">
				<option>Indonesia</option>
				<option>Singapore</option>
				<option>Malaysia</option>
				<option>Australia</option>
			</select>
			<p>Language Spoken :</p>
			<input type="checkbox" id="bahasa" name="spoken" value="bahasaindonesia">Bahasa Indonesia<br>
			<input type="checkbox" id="eglish" name="spoken" value="english">English<br>
			<input type="checkbox" id="other" name="spoken" value="other">Other<br>
			<p>Bio :</p>
			<textarea name="bio" rows="10" cols="30">
			</textarea><br>
			<input type="Submit" name="signup" value="welcome">
		</form>
    </body>
</html>
