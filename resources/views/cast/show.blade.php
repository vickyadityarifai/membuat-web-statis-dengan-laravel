@extends('master')

@section('judul')
    Lihat Detail Data
@endsection

@section('content')
<h2>Lihat Data {{$cast->id}}</h2>
<h4>Nama : {{$cast->nama}}</h4>
<p>Umur : {{$cast->umur}}<br>Bio : {{$cast->bio}}</p>
@endsection